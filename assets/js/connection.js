const connection = {
  mode: 'global',
  address: {
    local: 'http://0.0.0.0:0',
    global: 'https://niikeeper.com/mvp_test/v0.4.2'
  },
  getAddress () {
    return this.address[this.mode]
  }
}

export default connection
