import {point, multiLineString, lineString} from '@turf/helpers';
import length from '@turf/length';
import connection from '~/assets/js/connection';

export const editor = {
  methods: {
    editorInit() {
      if (this.viewMode) {
        this.editorInitViewMode();
        return;
      }
      ['layer-editor-allStops', 'layer-editor-allStops-names', 'layer-editor-allStops-cluster'].forEach(layer => {
        this.map.on('mouseenter', layer, (e) => {
          editorData.allStopsStopHover = true;
          this.map.getCanvas().style.cursor = 'pointer';
          this.showEditorAllStopsHoverPopup(e);
          editorData.allStopsStopHoverPopupShown = true;
        });
        this.map.on('mouseleave', layer, () => {
          editorData.allStopsStopHover = false;
          this.map.getCanvas().style.cursor = 'default';
          if (editorData.allStopsStopHoverPopupShown) {
            this.$bus.$emit('hideInfoHoverModal');
            editorData.allStopsStopHoverPopupShown = false;
          }
        });
        // this.map.on('click', layer, this.editorAllStopsClick);
      });

      ['layer-editor-allStops_mini'].forEach(layer => {
        this.map.on('click', layer, this.editorAllMiniStopsClick);
      });

      if (this.routeID) {
        this.generateStopsList();
        this.$bus.$emit('fitBounds', {
          coordinates: this.routesByID.reduce((acc, curr) => [...acc, ...curr.geometry.coordinates], []).reduce((acc, curr) => [...acc, ...curr], []),
        }, this.proposalModal ? 'proposalModal' : 3);
      }

      this.map.on('click', async (e) => {
        if (editorData.allStopsStopHover || editorData.stopsHover) {
          return;
        }
        const focusedStop = editorData.focusedStop;
        if (typeof focusedStop !== 'number') {
          return;
        }
        const stop = editorData.stopsList[editorData.activeRoute].stops[focusedStop];
        if (!stop || (stop && stop.type !== 'master' && !stop.coordinates)) {
          let newUserStop;
          if (this.cablewayEdit) {
            newUserStop = point([e.lngLat.lng, e.lngLat.lat]);
          } else {
            const query = `${connection.getAddress()}/directions/v1/closestPoint/${e.lngLat.lng},${e.lngLat.lat}`;
            const newCoordinatesData = await this.$axios.$get(query);
            newUserStop = point(newCoordinatesData.waypoint.location);
          }
          editorData.focusedStop = null;
          newUserStop.ind = focusedStop;
          newUserStop.layer = {
            id: 'layer-editor-stops',
            newUserStop: true,
          };
          newUserStop.properties.icon = 'ngpt-green';
          newUserStop.properties.iconImage = 'local-ngpt-green';
          newUserStop.properties.iconSize = .3;
          newUserStop.properties.newUserStop = true;
          editorData.newUserStop = newUserStop;
          this.addEditorStops({
            skipSaveHistory: true
          });
          this.$bus.$emit('fitMap', newUserStop.geometry.coordinates, this.map.getZoom());
          this.$bus.$emit('showMapPopup', {
            popup: 'stops',
            item: newUserStop,
            containerClassNames: ['editor-popup'],
          });
        }
      });
      [
        'layer-editor-stops-main-route',
        'layer-editor-stops-back-route',
        'layer-editor-stops-name',
        'layer-editor-stops-cluster',
        'layer-editor-edge-stops-main-route',
        'layer-editor-edge-stops-back-route',
        'layer-editor-green-stops-main-route',
        'layer-editor-green-stops-back-route'
      ].forEach(layer => {
        this.map.on('mouseenter', layer, () => {
          editorData.stopsHover = true;
          this.map.getCanvas().style.cursor = 'pointer'
        });
        this.map.on('mouseleave', layer, () => {
          editorData.stopsHover = false;
          this.map.getCanvas().style.cursor = 'default'
        });
        // this.map.on('click', layer, this.editorStopsClick);
      });
      // this.map.on('click', 'layer-editor-linePoints', this.editorLinePointsClick);
      ['layer-editor-green-stops-main-route', 'layer-editor-green-stops-back-route'].forEach(layer => {
        this.map.on('mousedown', layer, e => {
          e.preventDefault();
          const item = e.features[0];
          if (item.properties.stopType === 'user') {
            this.map.getCanvas().style.cursor = 'move';
            editorData.activeUserStop = {
              id: item.properties.id,
              ind: null,
              routeName: item.properties.routeName
            };
          }
          if (item.properties.stopType === 'master') {
            editorData.activeMasterStop = true;
          }
        });
      });
      this.map.on('mousedown', 'layer-editor-lines', this.editorLinesOnMouseDown);
      this.map.on('mousemove', (e) => {
        if (editorData.activeUserStop) {
          this.editorUserStopOnMove(e);
        }
      });
      this.map.on('mouseup', e => {
        if (editorData.activeUserStop) {
          this.editorUserStopOnMouseUp(e);
        }
        editorData.activeMasterStop = false;
      });
      this.addMetroStations();
      this.addJdStations();
      this.addMcdStations();
      this.addAllStops();
      this.addEditorStops();
    },
    startRoutePropertiesInit() {
      Object.keys(editorData.routeProperties).forEach(routeName => {
        if (!startRouteProperties[routeName]) {
          startRouteProperties[routeName] = {...editorData.routeProperties[routeName]}
        }
      })
    },
    centerRoute() {
      const stops = this.map.getSource('source-editor-stops')._data.features;
      const lines = this.map.getSource('source-editor-lines')._data.features;
      let coordinates = [];
      stops.forEach(s => coordinates.push(s.geometry.coordinates));
      lines.forEach(route => {
        if (route.geometry.type === 'LineString') {
          coordinates = coordinates.concat(route.geometry.coordinates);
        }
        if (route.geometry.type === 'MultiLineString') {
          coordinates = coordinates.concat(route.geometry.coordinates.reduce((acc, curr) => acc.concat(curr), []));
        }
      });
      let padding = 3;
      if (this.operator) {
        padding = 'operator';
      }
      if (this.proposalModal) {
        padding = 'proposalModal';
      }
      if (coordinates.length) {
        this.$bus.$emit('fitBounds', {
          coordinates
        }, padding);
      }
    },
    showEditorAllStopsHoverPopup(e) {
      const item = e.features[0];
      item.layer.allStopHover = true;
      this.$bus.$emit('showMapHoverPopup', {
        layer: 'editor',
        item,
        containerClassNames: ['editor-popup'],
        popup: {
          closeButton: false
        }
      });
    },
    editorAllMiniStopsClick(e) {
      if (!this.cablewayEdit) {
        this.map.flyTo({
          center: e.lngLat,
          zoom: 16
        });
        // this.editorAllStopsClick(e)
      }
    },
    editorAllStopsClick(e) {
      if (!this.cablewayEdit) {
        // if (typeof editorData.focusedStop === 'number') {
        //   console.log('break by data type')
        //   return;
        // }
        if (editorData.viewMode) {
          return;
        }
        this.editorData.allStopsStopHoverPopupShown = false;
        if (editorData.stopsHover) {
          return;
        }
        const item = e.features[1] || e.features[0];
        if (this.editorData.stopsList[this.editorData.activeRoute].stops.find(s => s && s.value === item.properties.id)) {
          return;
        }
        this.$bus.$emit('showMapPopup', {
          popup: 'stops',
          item,
          containerClassNames: ['editor-popup'],
        });
      }
    },
    editorStopsClick(e) {
      clearTimeout(this.editorStopsClickedItemsTimeOut);
      if (!this.editorData.cablewayEdit) {
        if (!this.editorStopsClickedItems) {
          this.editorStopsClickedItems = [];
        }
        this.editorStopsClickedItems.push(e.features[0]);
        this.editorStopsClickedItemsTimeOut = setTimeout(() => {
          let item = this.editorStopsClickedItems.find(s => s.properties.routeName === this.editorData.activeRoute);
          if (!item) {
            item = this.editorStopsClickedItems[0];
          }
          this.editorStopsClickedItems = [];
          if (item.properties.newUserStop) {
            item.layer = {
              id: 'layer-editor-stops',
              newUserStop: true,
            };
            editorData.newUserStop = item;
            this.addEditorStops({
              skipSaveHistory: true
            });
          }
          this.$bus.$emit('showMapPopup', {
            popup: 'stops',
            item,
            containerClassNames: ['editor-popup'],
          });
        });
      }
    },
    editorLinesOnMouseDown(e) {
      e.preventDefault();
      if (editorData.activeUserStop || editorData.activeMasterStop) {
        return;
      }
      if (editorData.activeRoute !== e.features[0].properties.routeName) {
        editorData.activeRoute = e.features[0].properties.routeName;
        this.addEditorStops({
          skipSaveHistory: true
        });
      }
    },
    editorUserStopOnMove(e) {
      e.preventDefault();
      const data = this.map.getSource('source-editor-stops')._data;
      if (editorData.activeUserStop.ind === null) {
        editorData.activeUserStop.ind = data.features.findIndex(s => s.id === editorData.activeUserStop.id);
      }
      data.features[editorData.activeUserStop.ind].geometry.coordinates = [e.lngLat.lng, e.lngLat.lat];
      this.map.getSource('source-editor-stops').setData(data);
      editorData.activeUserStop.moved = true;
    },
    async editorUserStopOnMouseUp(e) {
      e.preventDefault();
      this.map.getCanvas().style.cursor = 'default';
      if (editorData.activeUserStop.moved) {
        let newCoordinatesData = [e.lngLat.lng, e.lngLat.lat];
        if (!this.cablewayEdit) {
          const query = `${connection.getAddress()}/directions/v1/closestPoint/${e.lngLat.lng},${e.lngLat.lat}`;
          const queryResult = await this.$axios.$get(query);
          newCoordinatesData = queryResult.waypoint.location;
        }
        this.editorData.stopsList.mainRoute.stops.forEach(s => {
          if (s && s.id === editorData.activeUserStop.id) {
            s.coordinates = newCoordinatesData;
          }
        });
        this.editorData.stopsList.backRoute.stops.forEach(s => {
          if (s && s.id === editorData.activeUserStop.id) {
            s.coordinates = newCoordinatesData;
          }
        });
        editorData.userStopsById[editorData.activeUserStop.id].coordinates = newCoordinatesData;
        Object.values(editorData.routesList).forEach(routeName => { // update route coordinates
          Object.keys(routeName).forEach(route => {
            if (route.includes(editorData.activeUserStop.id)) {
              delete routeName[route];
            }
          });
        });
        editorData.activeUserStop = null;
        this.addEditorStops({
          skipSaveHistory: true
        });
      } else {
        editorData.activeUserStop = null;
      }
    },
    generateStopsList() {
      this.editorData.stopsList.mainRoute.stops = [];
      this.editorData.stopsList.backRoute.stops = [];
      let mainRoute = null;
      let backRoute = null;
      const routesID = this.routesByID ? this.routesByID.map(route => route.id) : [];

      this.routesByID.forEach(route => {
        if (route.properties.dir === 0) {
          mainRoute = JSON.parse(JSON.stringify(route));
          mainRoute.properties.routeName = 'mainRoute';
        } else {
          backRoute = JSON.parse(JSON.stringify(route));
          backRoute.properties.routeName = 'backRoute';
        }
      });
      this.stopsByRouteID.forEach(stop => {
        if (routesID.includes(stop.properties.routeGUID)) {
          if (stop.properties.dir === 0) {
            this.editorData.stopsList.mainRoute.stops.push({
              text: stop.properties.name,
              type: "master",
              value: stop.id,
              sequenceID: stop.properties.sequenceID
            })
          } else {
            this.editorData.stopsList.backRoute.stops.push({
              text: stop.properties.name,
              type: "master",
              value: stop.id,
              sequenceID: stop.properties.sequenceID
            })
          }
        }
      });

      this.editorData.stopsList.mainRoute.stops.sort((a, b) => a.sequenceID - b.sequenceID);
      this.editorData.stopsList.backRoute.stops.sort((a, b) => a.sequenceID - b.sequenceID);
      if (mainRoute) {
        mainRoute.distance = mainRoute.properties.length;
        mainRoute.duration = (mainRoute.distance / 1000 / 16.6) * 60 * 60;
        const lineId = this.editorData.stopsList.mainRoute.stops.map(s => s.value).join('-');
        mainRoute.properties.id = lineId;
        this.editorData.routesList.mainRoute[lineId] = mainRoute;
      }
      if (backRoute) {
        backRoute.distance = backRoute.properties.length;
        backRoute.duration = (backRoute.distance / 1000 / 16.6) * 60 * 60;
        const lineId = this.editorData.stopsList.backRoute.stops.map(s => s.value).join('-');
        backRoute.properties.id = lineId;
        this.editorData.routesList.backRoute[lineId] = backRoute;
      }
    },
    async getRouteLine({stops, routeName, updateLines, noUpdateLineGeometry}) {
      if (stops.length < 2) {
        return;
      }
      if ((this.operator || this.editorData.viewMode) && this.editorData.routesList[routeName].viewMode) {
        const result = this.editorData.routesList[routeName].viewMode;
        if (result) {
          result.distance = length(result.geometry) * 1000
        }
        return result;
      }
      stops = stops.filter(stop => stop && !stop.properties.old);
      const lineName = stops.map(s => s.id || s.properties.id).join('-');
      if (updateLines || !this.editorData.routesList[routeName][lineName]) {
        const lineData = await this.getLineFromApi(stops.map(s => s.geometry.coordinates), routeName);
        if (noUpdateLineGeometry && this.editorData.routesList[routeName][lineName]) {
          lineData.geometry = JSON.parse(JSON.stringify(this.editorData.routesList[routeName][lineName].geometry));
          lineData.distance = JSON.parse(JSON.stringify(this.editorData.routesList[routeName][lineName].distance));
          lineData.duration = lineData.distance / 1000 / lineData.speed * 60 * 60;
        }
        lineData.properties.id = lineName;
        this.editorData.routesList[routeName][lineName] = lineData;
        this.editorData.routeProperties.districts = lineData.properties.districts;
        this.editorData.routeProperties.divisions = lineData.properties.divisions;
        this.editorData.routeProperties.metroStations = lineData.properties.metroStations;
        if (!this.cablewayEdit) {
          this.editorData.routeProperties.desiredFrom = stops[0].properties.name;
          this.editorData.routeProperties.desiredTo = stops[stops.length - 1].properties.name;
        }
      }
      return this.editorData.routesList[routeName][lineName];
    },

    async getLineFromApi(coordinates, routeName) {
      const color = routeName === 'mainRoute' ? '#0080ff' : '#008000';
      const query = `${connection.getAddress()}/directions/v1/buses/${coordinates.join(';')}`;
      const params = {};
      if (editorData.description.weekDays !== 'Все' && editorData.description.weekDays) {
        params.desiredDays = editorData.description.weekDaysItems.find(i => i.text === this.editorData.description.weekDays).name;
      }
      if (editorData.description.dayTime !== 'Любое' && editorData.description.dayTime) {
        params.desiredTime = editorData.description.dayTimeItems.find(i => i.text === this.editorData.description.dayTime).name;
      }
      if (!this.routeID) {
        this.$bus.$emit('setSimilarRoutesStatus', {title: 'Построение маршрута', status: 'mapmatching'})
      }
      const data = await this.$axios.$get(query, {params});
      if (data.routes && data.routes[0]) {
        if (data.routes[0].geometry && data.routes[0].geometry.coordinates) {
          data.routes[0].geometry.coordinates = data.routes[0].geometry.coordinates.filter(c => c);
        }
        data.routes[0].properties = {
          color,
          routeName,
          districts: data.districts || [],
          divisions: data.divisions || [],
          metroStations: data.metroStations || []
        };
        if (this.cablewayEdit) {
          let line = lineString(coordinates);
          data.routes[0].geometry = line.geometry;
          data.routes[0].distance = length(line) * 1000;
          data.routes[0].duration = data.routes[0].distance / 1000 / 16.6 * 60 * 60;
          data.routes[0].speed = 16.6;
        }
        return data.routes[0]
      }
      console.error('getMatch error');
      return multiLineString(coordinates, {
        color,
        routeName
      });
    },
    saveHistory() {
      console.info('saveHistory');
      this.editorHistory.state.push({
        time: new Date(),
        editorData: JSON.parse(JSON.stringify(this.editorData))
      });
      if (this.editorHistory.ind === null) {
        this.editorHistory.ind = 0;
      } else {
        this.editorHistory.ind = this.editorHistory.state.length - 1;
      }
    },
  },
  computed: {
    cablewayEdit() {
      return this.$store.state.editor.cablewayEdit;
    },
    routes() {
      return [...this.$store.state.ngpt.routes];
    },
    route() {
      if (this.routeID) {
        return this.$store.state.ngpt.routes.find(r => r.routeID.toString() === this.routeID.toString());
      }
      return []
    },
    stops() {
      return this.$store.state.ngpt.stops;
    },
    proposals() {
      return this.$store.state.proposals.allProposals;
    },
    proposal() {
      return this.proposals.find(p => p.id === this.viewMode);
    },
    proposalInfo() {
      return this.$store.state.proposals.proposal;
    },
    stopsByRouteID() {
      return this.$store.state.ngpt.stopByRouteID;
    },
    routesByID() {
      return this.$store.state.ngpt.routesByID;
    },
  }
};

const editorData = {
  newProposal: {
    title: '',
    reason: '',
  },
  districts: [],
  divisions: [],
  metroStations: [],
  viewMode: false,
  segmentsListGenerated: false,
  focusedStop: null,
  allStopsStopHoverPopupShown: false,
  allStopsStopHover: false,
  stopsHover: false,
  masterRoute: null,
  userStopsById: {},
  activeRoute: 'mainRoute',
  stopsList: {
    mainRoute: {
      name: 'Прямой маршрут',
      stops: []
    },
    backRoute: {
      name: 'Обратный маршрут',
      stops: []
    },
  },
  routesList: {
    mainRoute: {},
    backRoute: {},
  },
  segmentsList: {
    mainRoute: {},
    backRoute: {},
  },
  routeProperties: {
    from: {
      type: 'text',
      title: 'От',
      value: null,
    },
    to: {
      type: 'text',
      title: 'До',
      value: null,
    },
    intervalMinutes: {
      type: 'text',
      title: 'Интервал движения',
      value: null,
      unit: 'мин'
    },
    vehiclesNum: {
      type: 'text',
      title: 'Количество ТС на маршруте',
      value: null,
    },
    stopsNum: {
      type: 'text',
      title: 'Количество остановок',
      value: null,
      unit: 'шт'
    },
    lengthKm: {
      type: 'text',
      title: 'Общая длина маршрута',
      value: null,
      unit: 'км'
    },
    time: {
      type: 'text',
      title: 'Общее время в пути',
      value: null,
      unit: 'мин'
    },
    mainRouteLengthKm: {
      type: 'text',
      title: 'Длина прямого маршрута',
      value: null,
      unit: 'км'
    },
    mainRouteSpeed: {
      type: 'text',
      title: 'Скорость движения в прямом направлении',
      value: null,
      unit: 'км/ч'
    },
    mainRouteTime: {
      type: 'text',
      title: 'Время в пути прямого маршрута',
      value: null,
      unit: 'мин'
    },
    backRouteLengthKm: {
      type: 'text',
      title: 'Длина обратного маршрута',
      value: null,
      unit: 'км'
    },
    backRouteSpeed: {
      type: 'text',
      title: 'Скорость движения в обратном направлении',
      value: null,
      unit: 'км/ч'
    },
    backRouteTime: {
      type: 'text',
      title: 'Время в пути обратного маршрута',
      value: null,
      unit: 'мин'
    },
  },
  description: {
    type: 'standart',
    routeStart: '',
    routeFinish: '',
    weekDays: 'Все',
    weekDaysItems: [
      {text: 'Все', name: 'any', value: 'Все'},
      {text: 'Выходные', name: 'weekends', value: 'Выходные'},
      {text: 'Будни', name: 'weekdays', value: 'Будни'},
    ],
    dayTime: 'Любое',
    dayTimeItems: [
      {text: 'Любое', name: 'any', value: 'Любое'},
      {text: 'Утро', name: 'mor', value: 'Утро'},
      {text: 'День', name: 'aft', value: 'День'},
      {text: 'Вечер', name: 'eve', value: 'Вечер'},
      {text: 'Ночь', name: 'night', value: 'Ночь'},
    ],
    typeTS: 'Любой',
    typeTSItems: [
      {text: 'Любой', value: 'Любой'},
      {text: 'Электробус', value: 'Электробус'},
      {text: 'Большой вместимости', value: 'БВ'},
      {text: 'Малой вместимости', value: 'МВ'},
    ],
    interval: '20',
  },
  buildings3D: false,
  blockStopClick: false,
  showOriginalMasterCopy: false,
  currentTS: {
    routesByStopID: {},
    routes: {},
    devices: {},
    features: [],
    featuresToDraw: [],
    status: "halted",
    count: 0,
    show: false
  },
};

export const ngptOptions = {
  bus: {
    lineColor: '#0080ff',
    lineColor2: '#42a1ff',
    lineColorDark: '#008000',
    lineColorDark2: '#00bd00',
    lineColorDarkName: 'lineColorDark-bus',
    lineOpacity: 1,
    lineColorLegend: '#d32f2f',
    lineOpacityLegend: 1,
    lineWidth: 2,
    lineWidthLegend: 4,
    name: 'Маршрут автобуса',
  },
  trolleybus: {
    lineColor: '#0194d3',
    lineColorDark: '#01649b',
    lineColorDarkName: 'lineColorDark-trolleybus',
    lineOpacity: 1,
    lineColorLegend: '#01b5ff',
    lineOpacityLegend: 1,
    lineWidth: 2,
    lineWidthLegend: 4,
    name: 'Маршрут троллейбуса',
  },
  tram: {
    lineColor: '#F5A623',
    lineColorDark: '#9b6e18',
    lineColorDarkName: 'lineColorDark-tram',
    lineOpacity: 1,
    lineColorLegend: '#ffae23',
    lineOpacityLegend: 1,
    lineWidth: 2,
    lineWidthLegend: 4,
    name: 'Маршрут трамвая',
  },
  historyColors: [
    '#000',
    '#9d9d9d',
    '#7f7699',
  ]
};

export const editorHistory = {
  ind: null,
  state: [],
};

export const startRouteProperties = {};

export default editorData;
