import connection from '~/assets/js/connection';

// Телематика
const defaultState = {
  loading: false,
  telematics: [],
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_TELEMATICS(state, data) {
    state.telematics = data;
  },
  SET_LOADING(state, data) {
    state.loading = data;
  },
  CLEAR_TELEMATICS_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
};

export const actions = {
  async getTelematics({ commit }) {
    commit('SET_LOADING', true);
    const polyline = require('@mapbox/polyline');
    const point = require('@turf/helpers').point;
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/vehicles/telematics`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    const decodedData = data.data.map(itemData => {
      return point(polyline.decode(itemData.geom)[0], {
        bearing: itemData.bearing,
        device_id: itemData.device_id,
        company: itemData.company.replace('@', ''),
        velocity: itemData.velocity,
        licensePlate: itemData.licensePlate,
        routeName: itemData.routeName ? itemData.routeName : 'н/д',
        routeId: itemData.routeId,
        timestamp: itemData.timestamp
      });
    });
    commit('SET_TELEMATICS', decodedData);
    commit('SET_LOADING', false);
  },
  clearTelematicsStore({ commit }) {
    commit('CLEAR_TELEMATICS_STORE')
  }
};
