import connection from '~/assets/js/connection';

const defaultState = {
  proposals: [],
  summary: {
    login: '',
    username: ''
  },
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_PROPOSALS(state, proposals) {
    state.proposals = proposals;
  },
  SET_SUMMARY(state, summary) {
    state.summary = summary;
  },
  CLEAR_ACCOUNT_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key];
    })
  },
  CLEAR_PROPOSALS(state) {
    state.proposals = defaultState.proposals;
  },
};

export const actions = {
  async getProposals({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/moscow/proposals/sec/account/data/proposals`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_PROPOSALS', data.data);
  },
  async getSummary({ commit }) {
    const { data } = await this.$axios.get(
      `${connection.getAddress()}/account/profile/settings/common/view/summary`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    );
    commit('SET_SUMMARY', data.data);
  },
  editUserName({ commit }, data) {
    return this.$axios.post(
      `${connection.getAddress()}/account/profile/settings/common/edit/username`,
      data,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    )
},
  async editPw({ commit }, data) {
    return this.$axios.post(
      `${connection.getAddress()}/account/profile/settings/security/edit/changepw`,
      data,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        },
      }
    )
  },
  clearAccountStore({ commit }) {
    commit('CLEAR_ACCOUNT_STORE')
  },
  clearProposals({ commit }) {
    commit('CLEAR_PROPOSALS')
  }
};
