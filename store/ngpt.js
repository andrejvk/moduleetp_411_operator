import connection from '~/assets/js/connection';

// Маршрутная сеть
const defaultState = {
  routes: [],
  stops: [],
  stopsById: {},
  routesByName: {},
  routesByID: [],
  stopByRouteID: []
};

export const state = () => ({
  ...defaultState
});

export const mutations = {
  SET_ROUTES(state, data) {
    state.routes = data
  },
  SET_ALL_ROUTES(state, data) {
    state.allRoutes = data
  },
  SET_STOPS(state, data) {
    state.stops = data
  },
  SET_STOPS_BY_ID(state, data) {
    state.stopsById = data.reduce((acc, curr) => {
      acc[curr.id] = curr;
      return acc;
    }, {});
  },
  SET_ALL_STOPS_BY_ID(state, data) {
    state.allStopsById = data.reduce((acc, curr) => {
      acc[curr.id] = curr;
      return acc;
    }, {});
  },
  SET_ROUTES_BY_NAME(state, data) {
    const routesByName = {};
    data.forEach(route => {
      if (!routesByName[route.name]) {
        routesByName[route.name] = {
          [route.dir]: route
        }
      } else {
        routesByName[route.name][route.dir] = route;
      }
    });
    state.routesByName = routesByName;
  },
  SET_ROUTES_BY_GUID(state, data) {
    const routesByGUID = {};
    data.forEach(route => {
      routesByGUID[route.id] = route
    });
    state.routesByGUID = routesByGUID;
  },
  SET_ROUTES_BY_ID(state, data) {
    state.routesByID = data
  },
  SET_STOPS_BY_ROUTE_ID(state, data) {
    state.stopByRouteID = data
  },
  CLEAR_NGPT_STORE(state) {
    Object.keys(defaultState).forEach(key => {
      state[key] = defaultState[key]
    })
  }
}

export const actions = {
  async getRoutes({commit}) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/ngpt/routes/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_ROUTES_BY_NAME', data.data)
    commit('SET_ROUTES_BY_GUID', data.data)
    commit('SET_ROUTES', data.data);
  },
  async getStops({commit}) {
    const polyline = require('@mapbox/polyline');
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/ngpt/stops/list`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    const decodedData = data.data.map(item => ({
      id: item.id,
      geometry: {
        type: 'Point',
        coordinates: polyline.decode(item.geom)[0].reverse()
      },
      properties: {
        name: item.name,
        fullname: item.fullname,
        id: item.id,
        stopID: item.stopID
      },
      type: 'Feature',
    }))
    commit('SET_STOPS', decodedData)
    commit('SET_STOPS_BY_ID', decodedData)
  },
  async getRouteData({commit, state}, routeID) {
    await Promise.all([
      this.$axios.get(
        `${connection.getAddress()}/moscow/ngpt/routes/byRouteID/${routeID}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        }
      ),
      this.$axios.get(
        `${connection.getAddress()}/moscow/ngpt/stops/byRouteID/${routeID}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        }
      )
    ]).then(([routesData, stopsData]) => {
      let mainRoute;
      let backRoute;
      routesData.data.features.forEach(route => {
        if (route.properties.dir === 0) {
          mainRoute = route;
          mainRoute.properties.color = '#0080ff';
        }
        if (route.properties.dir === 1) {
          backRoute = route;
          backRoute.properties.color = '#008000';
        }
      });
      commit('SET_ROUTES_BY_ID', routesData.data.features);
      stopsData.data.features.forEach(stop => {
        if (stop.properties.dir === 0) {
          stop.properties.routeID = mainRoute.id;
        }
        if (stop.properties.dir === 1) {
          stop.properties.routeID = backRoute.id;
        }
      });
      const stops = state.stops.concat(stopsData.data.features);
      commit('SET_STOPS', stops)
      commit('SET_STOPS_BY_ID', stops)
      commit('SET_STOPS_BY_ROUTE_ID', stopsData.data.features)
    });
  },
  async getRouteByRouteID({commit}, routeID) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/ngpt/routes/byRouteID/${routeID}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_ROUTES_BY_ID', data.features);
  },
  async getStopByRouteID({commit}, routeID) {
    const {data} = await this.$axios.get(
      `${connection.getAddress()}/moscow/ngpt/stops/byRouteID/${routeID}`,
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      }
    )
    commit('SET_STOPS_BY_ROUTE_ID', data.features)
  },
  clearNgptStore({commit}) {
    commit('CLEAR_NGPT_STORE')
  }
}
